DELIVARABLES CHECK
----------------------
1. Landing Page Design -- Done
2. Site User Authentication (Normal Login) -- Done
3. Registered User Email Verification  -- Done
4. Reset Password --Done
5. New User Signup -- Done
6. Change Password -- Done
7. User Profile -- Done
6. Social User authentication via Facebook  --- Done


USEFUL URLS
----------------------
Landing Page Design: http://localhost/sb_restaurant/web/app_dev.php/ 

Site User Authentication (Normal Login): http://localhost/sb_restaurant/web/app_dev.php/shop/login

Reset Password:  http://localhost/sb_restaurant/web/app_dev.php/shop/resetting/request

New User Signup: http://localhost/sb_restaurant/web/app_dev.php/shop/register/

Change Password: http://localhost/sb_restaurant/web/app_dev.php/shop/change_password/change-password

User Profile: http://localhost/sb_restaurant/web/app_dev.php/shop/profile



SQL CHANGES
-----------------------
ALTER TABLE `client` ADD `facebook_id` VARCHAR(250) NULL AFTER `full_name`;



Play around it and Let me know your thoughts

-------------------------------------------------
LATEST SQL CHANGES:
-------------------------------------------------

ALTER TABLE `dish` ADD `shop` INT(11) NOT NULL AFTER `category`;

--
-- Table structure for table `shop`
--

CREATE TABLE `shop` (
  `shop_id` int(14) NOT NULL,
  `shop_name` varchar(255) NOT NULL,
  `shop_description` longtext,
  `shop_logo` varchar(1000) DEFAULT 'shop_logo.jpg',
  `shop_location` varchar(400) NOT NULL,
  `shop_zipcode` varchar(50) NOT NULL,
  `shop_createdate` datetime NOT NULL,
  `shop_lastupdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`shop_id`),
  ADD UNIQUE KEY `name` (`shop_name`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `shop_id` int(14) NOT NULL AUTO_INCREMENT;
  
  
  
  
--
-- Table structure for table `special_offers`
--

CREATE TABLE `special_offers` (
  `special_offer_id` int(14) NOT NULL,
  `special_offer_title` varchar(255) NOT NULL,
  `special_offer_description` varchar(1000) DEFAULT NULL,
  `special_offer_dish` int(14) NOT NULL,
  `special_offer_banner` varchar(100) DEFAULT NULL,
  `special_offer_status` varchar(1) NOT NULL DEFAULT '1',
  `special_offer_createdate` datetime NOT NULL,
  `special_offer_lastupdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `special_offers`
--
ALTER TABLE `special_offers`
  ADD PRIMARY KEY (`special_offer_id`),
  ADD KEY `special_offer_dish` (`special_offer_dish`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `special_offers`
--
ALTER TABLE `special_offers`
  MODIFY `special_offer_id` int(14) NOT NULL AUTO_INCREMENT;
  
  
  
--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `favorite_id` int(14) NOT NULL,
  `favorite_client` int(14) NOT NULL,
  `favorite_dish` int(14) NOT NULL,
  `favorite_createdate` datetime NOT NULL,
  `favorite_lastupdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`favorite_id`),
  ADD KEY `favorite_client` (`favorite_client`),
  ADD KEY `favorite_dish` (`favorite_dish`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `favorite_id` int(14) NOT NULL AUTO_INCREMENT;