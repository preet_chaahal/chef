<?php

namespace Su\RestaurantBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Shop
 *
 * @ORM\Table(name="shop", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"shop_name"})})
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks
 */
class Shop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $shopId;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_name", type="string", length=255, nullable=false)
     */
    private $shopName;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_description", type="string", nullable=true)
     */
    private $shopDescription;
	
	/**
     * @var string
     *
     * @ORM\Column(name="shop_logo", type="string", length=1000, nullable=true)
	 * @Assert\Valid()
     */
    private $shopLogo;
	
	/**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Assert\File(maxSize="4M",mimeTypes={"image/png", "image/jpeg", "image/pjpeg"})
     *
     * @Vich\UploadableField(mapping="shop_logo", fileNameProperty="shop_logo")
     * @var File
     */
    private $shopLogoFile;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_location", type="string", length=400,nullable=false)
     */
    private $shopLocation;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_zipcode", type="string", nullable=false)
     */
    private $shopZipcode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="shop_createdate", type="datetime", nullable=false)
     */
    private $shopCreatedate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="shop_lastupdate", type="datetime", nullable=true)
     */
    private $shopLastupdate;


    public function __construct(){
        //
        $this->setShopCreatedate(new \DateTime('now'));

    }


    /**
     * Get shopId
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * Set shopName
     *
     * @param string $shopName
     *
     * @return Shop
     */
    public function setShopName($shopName)
    {
        $this->shopName = $shopName;

        return $this;
    }

    /**
     * Get shopName
     *
     * @return string
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * Set shopDescription
     *
     * @param string $shopDescription
     *
     * @return Shop
     */
    public function setShopDescription($shopDescription)
    {
        $this->shopDescription = $shopDescription;

        return $this;
    }

    /**
     * Get shopDescription
     *
     * @return string
     */
    public function getShopDescription()
    {
        return $this->shopDescription;
    }
	
	/**
     * Set shopLogo
     *
     * @param string $shopLogo
     *
     * @return Shop
     */
    public function setshopLogo($shopLogo)
    {
        $this->shopLogo = $shopLogo;

        return $this;
    }

    /**
     * Get shopLogo
     *
     * @return string
     */
    public function getshopLogo()
    {
        return $this->shopLogo;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $shopLogo
     *
     * @return Shop
     */
    public function setshopLogoFile(File $shopLogo = null)
    {
        $this->shopLogoFile = $image;

        if ($shopLogo) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->shopLastupdate = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getshopLogoFile()
    {
        return $this->shopLogoFile;
    }
    

    /**
     * Set shopLocation
     *
     * @param string $shopLocation
     *
     * @return Shop
     */
    public function setShopLocation($shopLocation)
    {
        $this->shopLocation = $shopLocation;

        return $this;
    }

    /**
     * Get shopLocation
     *
     * @return string
     */
    public function getShopLocation()
    {
        return $this->shopLocation;
    }
	
	/**
     * Set shopZipcode
     *
     * @param string $shopZipcode
     *
     * @return Shop
     */
    public function setShopZipcode($shopZipcode)
    {
        $this->shopZipcode = $shopZipcode;

        return $this;
    }

    /**
     * Get shopZipcode
     *
     * @return string
     */
    public function getShopZipcode()
    {
        return $this->shopZipcode;
    }

    /**
     * Set shopCreatedate
     *
     * @param \DateTime $shopCreatedate
     *
     * @return Shop
     */
    public function setShopCreatedate($shopCreatedate)
    {
        $this->shopCreatedate = $shopCreatedate;

        return $this;
    }

    /**
     * Get shopCreatedate
     *
     * @return \DateTime
     */
    public function getShopCreatedate()
    {
        return $this->shopCreatedate;
    }

    /**
     * Set shopLastupdate
     *
     * @param \DateTime $shopLastupdate
     *
     * @return Shop
     */
    public function setShopLastupdate($shopLastupdate)
    {
        $this->shopLastupdate = $shopLastupdate;

        return $this;
    }

    /**
     * Get shopLastupdate
     *
     * @return \DateTime
     */
    public function getshopLastupdate()
    {
        return $this->shopLastupdate;
    }


    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setShopLastupdate(new \DateTime('now'));

        if ($this->setShopCreatedate() == null) {
            $this->setShopCreatedate(new \DateTime('now'));
        }
    }

    public function __toString() {
        return $this->shopName;
    }
}
