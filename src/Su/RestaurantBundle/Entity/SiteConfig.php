<?php

namespace Su\RestaurantBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SiteConfig
 *
 * @ORM\Table(name="site_config", uniqueConstraints={@ORM\UniqueConstraint(name="shop", columns={"shop"})})
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks
 */
class SiteConfig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="string", length=500, nullable=false)
     */
    private $descr;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="taxrate", type="integer", nullable=true)
     */
    private $taxrate;
    
   	/**
     * @var float
     *
     * @ORM\Column(name="deliveryfee", type="float", precision=14, scale=2,  nullable=true)
     */
    private $deliveryfee;
        
   	/**
     * @var string
     *
     * @ORM\Column(name="otherval", type="string", length=100, nullable=true)
     */
    private $otherval;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="shop", type="integer", nullable=true)
     */
    private $shop;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;


    public function __construct(){
        //
        $this->setCreateDate(new \DateTime('now'));

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return SiteConfig
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set taxrate
     *
     * @param integer $taxrate
     *
     * @return SiteConfig
     */
    public function setTaxrate($taxrate)
    {
        $this->taxrate = $taxrate;

        return $this;
    }

    /**
     * Get taxrate
     *
     * @return integer
     */
    public function getTaxrate()
    {
        return $this->taxrate;
    }
	
	/**
     * Set deliveryfee
     *
     * @param float $deliveryfee
     *
     * @return SiteConfig
     */
    public function setDeliveryfee($deliveryfee)
    {
        $this->deliveryfee = $deliveryfee;

        return $this;
    }

    /**
     * Get deliveryfee
     *
     * @return float
     */
    public function getDeliveryfee()
    {
        return $this->deliveryfee;
    }
    
    /**
     * Set otherval
     *
     * @param string $otherval
     *
     * @return SiteConfig
     */
    public function setOtherval($otherval)
    {
        $this->otherval = $otherval;

        return $this;
    }

    /**
     * Get otherval
     *
     * @return string
     */
    public function getOtherval()
    {
        return $this->otherval;
    }

    /**
     * Set shop
     *
     * @param integer $shop
     *
     * @return SiteConfig
     */
    public function setShop($shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return integer
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Get CreateDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->CreateDate;
    }

    /**
     * Set CreateDate
     *
     * @param \DateTime $CreateDate
     *
     * @return SiteConfig
     */
    public function setCreateDate($CreateDate)
    {
        $this->CreateDate = $CreateDate;

        return $this;
    }

   /**
     * Get LastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->LastUpdate;
    }


    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setLastUpdate(new \DateTime('now'));

        if ($this->setCreateDate() == null) {
            $this->setCreateDate(new \DateTime('now'));
        }
    }

    public function __toString() {
        return $this->code;
    }
}
