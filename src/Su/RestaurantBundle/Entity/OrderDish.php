<?php

namespace Su\RestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
//use Su\RestaurantBundle\Entity\ClientOrder;

/**
 * OrderDish
 *
 * @ORM\Table(name="order_dish", indexes={@ORM\Index(name="dish", columns={"dish"})})
 * @ORM\Entity
 */
class OrderDish
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
        /**
     * @var integer
     *
     * @ORM\Column(name="dish", type="integer", nullable=true)
     */
    private $dish;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=256, nullable=false)
     */
    private $note;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=3, scale=2, nullable=false)
     */
    private $price;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="client_address", type="integer", nullable=true)
     */
    private $clientAddress;



    public function __construct(){
        //
        $this->setCreationDate(new \DateTime('now'));
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set dish
     *
     * @param integer $dish
     *
     * @return OrderDish
     */
    public function setDish($dish)
    {
        $this->dish = $dish;

        return $this;
    }
    
    /**
     * Get dish
     *
     * @return integer
     */
    public function getDish()
    {
        return $this->dish;
    }
    

    /**
     * Set note
     *
     * @param string $note
     *
     * @return OrderDish
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }
    
    /**
     * Set price
     *
     * @param float $price
     *
     * @return Dish
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }


    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return OrderDish
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return OrderDish
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     *
     * @return OrderDish
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set clientAddress
     *
     * @param integer $clientAddress
     *
     * @return OrderDish
     */
    public function setClientAddress($clientAddress)
    {
        $this->clientAddress = $clientAddress;

        return $this;
    }

    /**
     * Get clientAddress
     *
     * @return integer
     */ 
    public function getClientAddress()
    {
        return $this->clientAddress;
    }


    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdateDate(new \DateTime('now'));

        if ($this->setCreationDate() == null) {
            $this->setCreationDate(new \DateTime('now'));
        }
    }

}
