<?php

namespace Su\RestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

//use Su\RestaurantBundle\Entity\OrderDish;

/**
 * ClientAddress
 *
 * @ORM\Table(name="client_address", uniqueConstraints={@ORM\UniqueConstraint(name="client_name", columns={"client_name,email"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PaymentDetailAddress {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    private $clientId;
    private $payment_key;
    private $payment_type;
    private $payment_id;

   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function setPaymentKey($payment_key) {
        $this->payment_key = $payment_key;

        return $this;
    }

    public function getPaymentKey() {
        return $this->payment_key;
    }

    /**
     * Set address1
     *
     * @param string $address1
     *
     * @return ClientAddress
     */
    public function setPaymentType($payment_type) {
        $this->payment_type = $payment_type;

        return $this;
    }

    public function getPaymentType() {
        return $this->payment_type;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return ClientAddress
     */
    public function setPaymentId($payment_id) {
        $this->payment_id = $payment_id;

        return $this;
    }

    public function getPaymentId() {
        return $this->payment_id;
    }

    public function updatedTimestamps() {
        $this->setUpdateDate(new \DateTime());

        if ($this->setCreationDate() == null) {
            $this->setCreationDate(new \DateTime());
        }
    }

    /**
     * Set clientId
     *
     * @param integer $clientId
     *
     * @return ClientAddress
     */
    public function setClientId($clientId) {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return integer
     */
    public function getClientId() {
        return $this->clientId;
    }

}
