<?php

namespace Su\RestaurantBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Specialoffers
 *
 * @ORM\Table(name="specialoffers",  indexes={@ORM\Index(name="special_offer_dish", columns={"special_offer_dish"})})
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks
 */
class Specialoffers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="special_offer_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $specialOfferId;

    /**
     * @var string
     *
     * @ORM\Column(name="special_offer_title", type="string", length=255, nullable=false)
     */
    private $specialOfferTitle;
      /**
     * @var string
     *
     * @ORM\Column(name="special_offer_description", type="string", length=255, nullable=true)
     */
    private $specialOfferDescription;
    
    /**
     * @var float
     *
     * @ORM\Column(name="special_offer_price", type="float", precision=3, scale=2, nullable=false)
     */
    private $specialOfferPrice;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="special_offer_dish", type="integer", nullable=false)
     */
    private $specialOfferDish;

    /**
     * @var string
     *
     * @ORM\Column(name="special_offer_banner", type="string", length=100, nullable=true)
     * @Assert\Valid()
     */
    private $specialOfferBanner;
     /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Assert\File(maxSize="4M",mimeTypes={"image/png", "image/jpeg", "image/pjpeg"})
     *
     * @Vich\UploadableField(mapping="specialoffer_banner", fileNameProperty="image")
     * @var File
     */
    private $specialOfferBannerFile;

    /**
     * @var string
     *
     * @ORM\Column(name="special_offer_status", type="string", length=1, nullable=false)
	 *
     */
    private $specialOfferStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="special_offer_createdate", type="datetime", nullable=false)
     */
    private $specialOfferCreatedate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="special_offer_lastupdate", type="datetime", nullable=true)
     */
    private $specialOfferLastupdate;


    public function __construct(){
        //
        $this->setSpecialOfferCreationdate(new \DateTime('now'));

    }


    /**
     * Get specialOfferId
     *
     * @return integer
     */
    public function getSpecialOfferId()
    {
        return $this->dishId;
    }

    /**
     * Set  specialOfferTitle
     *
     * @param string $specialOfferTitle
     *
     * @return SpecialOffers
     */
    public function setSpecialOfferTitle($specialOfferTitle)
    {
        $this->specialOfferTitle = $specialOfferTitle;

        return $this;
    }

    /**
     * Get specialOfferTitle
     *
     * @return string
     */
    public function getSpecialOfferTitle()
    {
        return $this->specialOfferTitle;
    }

    /**
     * Set specialOfferDescription
     *
     * @param string $specialOfferDescription
     *
     * @return SpecialOffers
     */
    public function setSpecialOfferDescription($specialOfferDescription)
    {
        $this->specialOfferDescription = $specialOfferDescription;

        return $this;
    }

    /**
     * Get specialOfferDescription
     *
     * @return string
     */
    public function getSpecialOfferDescription()
    {
        return $this->specialOfferDescription;
    }
    
    /**
     * Set specialOfferPrice
     *
     * @param float $specialOfferPrice
     *
     * @return specialOffers
     */
    public function setSpecialOfferPrice($specialOfferPrice)
    {
        $this->specialOfferPrice = $specialOfferPrice;

        return $this;
    }

    /**
     * Get specialOfferPrice
     *
     * @return float
     */
    public function getSpecialOfferPrice()
    {
        return $this->specialOfferPrice;
    }

   /**
     * Set specialOfferDish
     *
     * @param integer $specialOfferDish
     *
     * @return SpecialOffer
     */
    public function setSpecialOfferDish($specialOfferDish)
    {
        $this->specialOfferDish = $specialOfferDish;

        return $this;
    }

    /**
     * Get specialOfferDish
     *
     * @return integer
     */
    public function getSpecialOfferDish()
    {
        return $this->specialOfferDish;
    }
    
     /**
     * Set specialOfferBanner
     *
     * @param string $specialOfferBanner
     *
     * @return SpecialOffers
     */
    public function setSpecialOfferBanner($specialOfferBanner)
    {
        $this->specialOfferBanner = $specialOfferBanner;

        return $this;
    }

    /**
     * Get specialOfferBanner
     *
     * @return string
     */
    public function getSpecialOfferBanner()
    {
        return $this->specialOfferBanner;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $specialOfferBanner
     *
     * @return SpecialOffers
     */
    public function setSpecialOfferBannerFile(File $specialOfferBanner = null)
    {
        $this->specialOfferBannerFile = $specialOfferBanner;

        if ($specialOfferBanner) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->specialOfferLastupdate = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getSpecialOfferBannerFile()
    {
        return $this->specialOfferBannerFile;
    }
    
    /**
     * Set  specialOfferStatus
     *
     * @param string $specialOfferStatus
     *
     * @return SpecialOffers
     */
    public function setspecialOfferStatus($specialOfferStatus)
    {
        $this->specialOfferStatus = $specialOfferStatus;

        return $this;
    }

    /**
     * Get specialOfferStatus
     *
     * @return string
     */
    public function getSpecialOfferStatus()
    {
        return $this->specialOfferStatus;
    }

    /**
     * Set specialOfferCreatedate
     *
     * @param \DateTime $specialOfferCreatedate
     *
     * @return SpecialOffers
     */
    public function setSpecialOfferCreatedate($specialOfferCreatedate)
    {
        $this->specialOfferCreatedate = $specialOfferCreatedate;

        return $this;
    }

    /**
     * Get specialOfferCreatedate
     *
     * @return \DateTime
     */
    public function getSpecialOfferCreatedate()
    {
        return $this->specialOfferCreationdate;
    }

    /**
     * Set specialOfferLastupdate
     *
     * @param \DateTime $specialOfferLastupdate
     *
     * @return SpecialOffers
     */
    public function setSpecialOfferLastupdate($specialOfferLastupdate)
    {
        $this->specialOfferLastupdate = $specialOfferLastupdate;

        return $this;
    }

    /**
     * Get specialOfferLastupdate
     *
     * @return \DateTime
     */
    public function getSpecialOfferLastupdate()
    {
        return $this->specialOfferLastupdate;
    }


    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setSpecialOfferLastupdate(new \DateTime('now'));

        if ($this->setSpecialOfferCreationdate() == null) {
            $this->setSpecialOfferCreationdate(new \DateTime('now'));
        }
    }

    public function __toString() {
        return $this->name;
    }
}
