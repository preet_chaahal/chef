<?php

namespace Su\RestaurantBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Favorites
 *
 * @ORM\Table(name="favorites",indexes={@ORM\Index(name="favorite_client", columns={"favorite_client"}), @ORM\Index(name="favorite_dish", columns={"favorite_dish"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Favorites
{
    /**
     * @var integer
     *
     * @ORM\Column(name="favorite_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $favoriteId;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="favorite_client", type="integer", length=14, nullable=false)
     */
    private $favoriteClient;

    /**
     * @var integer
     *
     * @ORM\Column(name="favorite_dish", type="integer", length=14, nullable=false)
     */
    private $favoriteDish;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="favorite_createdate", type="datetime", nullable=false)
     */
    private $favoriteCreatedate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="favorite_lastupdate", type="datetime", nullable=true)
     */
    private $favoriteLastupdate;


    public function __construct(){
        //
        $this->setFavoriteLastupdate(new \DateTime('now'));

    }


    /**
     * Get 	favoriteId
     *
     * @return integer
     */
    public function getFavoriteId()
    {
        return $this->favoriteId;
    }

	

    /**
     * Set favoriteClient
     *
     * @param integer $favoriteClient
     *
     * @return Favorites
     */
    public function setFavoriteClient($favoriteClient)
    {
        $this->favoriteClient = $favoriteClient;

        return $this;
    }

    /**
     * Get favoriteClient
     *
     * @return integer
     */
    public function getFavoriteClient()
    {
        return $this->favoriteClient;
    }
	
	
	/**
     * Set 	favoriteDish
     *
     * @param integer $favoriteDish
     *
     * @return Favorites
     */
    public function setFavoriteDish($favoriteDish)
    {
        $this->favoriteDish = $favoriteDish;

        return $this;
    }

    /**
     * Get favoriteDish
     *
     * @return integer
     */
    public function getFavoriteDish()
    {
        return $this->favoriteDish;
    }
	

    /**
     * Set favoriteCreatedate
     *
     * @param \DateTime $favoriteCreatedate
     *
     * @return Favorites
     */
    public function setFavoriteCreatedate($favoriteCreatedate)
    {
        $this->favoriteCreatedate = $favoriteCreatedate;

        return $this;
    }

    /**
     * Get favoriteCreatedate
     *
     * @return \DateTime
     */
    public function getFavoriteCreatedate()
    {
        return $this->favoriteCreatedate;
    }

    /**
     * Set 	favoriteLastupdate
     *
     * @param \DateTime $favoriteLastupdate
     *
     * @return Favorites
     */
    public function setFavoriteLastupdate($favoriteLastupdate)
    {
        $this->favoriteLastupdate = $favoriteLastupdate;

        return $this;
    }

    /**
     * Get favoriteLastupdate
     *
     * @return \DateTime
     */
    public function getFavoriteLastupdate()
    {
        return $this->favoriteLastupdate;
    }


    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setFavoriteLastupdate(new \DateTime('now'));

        if ($this->setFavoriteCreatedate() == null) {
            $this->setFavoriteCreatedate(new \DateTime('now'));
        }
    }

    
}
