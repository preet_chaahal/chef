<?php

namespace Su\RestaurantBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FavoritesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('favoriteDish', EntityType::class, array(
                'label' => 'Dish ',
                'class' => 'SuRestaurantBundle:Dish',
                'choice_label' => 'name',
                'choice_value' => 'dish_id',
            ))
			->add('favoriteClient', EntityType::class, array(
                'label' => 'Client',
                'class' => 'SuUserBundle:Client',
                'choice_label' => '	full_name',
                'choice_value' => 'id',
            ))
            //->add('name')
            //->add('description')
            //->add('price')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Su\RestaurantBundle\Entity\Favorites',
            'csrf_protection'   => false,
            'allow_extra_fields' => true
        ));
    }

    public function getName()
    {
        return 'su_restaurantbundle_favorites';
    }
}
