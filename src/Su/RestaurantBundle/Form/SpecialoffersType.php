<?php

namespace Su\RestaurantBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SpecialoffersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('specialOfferDish', EntityType::class, array(
                'label' => 'Dish ',
                'class' => 'SuRestaurantBundle:Dish',
                'choice_label' => 'name',
                'choice_value' => 'dish_id'
            ))
			->add('specialOfferStatus', ChoiceType::class, array(
                'label' => 'Display Status ',
                'choices'  => array(
                    'Enable' => '1',
                    'Disable' => '0'

                )
            ))
            //->add('name')
            //->add('description')
            //->add('price')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Su\RestaurantBundle\Entity\Specialoffers',
            'csrf_protection'   => false,
            'allow_extra_fields' => true
        ));
    }

    public function getName()
    {
        return 'su_restaurantbundle_specialoffers';
    }
}
