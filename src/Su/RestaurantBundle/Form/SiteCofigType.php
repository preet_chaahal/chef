<?php

namespace Su\RestaurantBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SiteCofigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->add('descr')
           ->add('code')
		   ->add('taxrate')
		   ->add('deliveryfee')
           ->add('otherval')
           ->add('shop', EntityType::class, array(
                'label' => 'Shop',
                'class' => 'SuRestaurantBundle:Shop',
                'choice_label' => 'shopName',
                'choice_value' => 'shopId',
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Su\RestaurantBundle\Entity\SiteCofig',
            'csrf_protection'   => false,
            'allow_extra_fields' => true
        ));
    }

    public function getName()
    {
        return 'su_restaurantbundle_siteconfig';
    }
}
