<?php

namespace Su\RestaurantBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
//use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DeliveryOrderaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status')
            ->add('clientOrderId')
            ->add('addressId')
            ->add('clientId')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
   {
        $resolver->setDefaults(array(
            'data_class' => 'Su\RestaurantBundle\Entity\DeliveryOrder',
            'csrf_protection'   => false,
            'allow_extra_fields' => true
        ));
    }

    public function getName()
    {
        return 'su_restaurantbundle_deliveryorder';
    }


}
