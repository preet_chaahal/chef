<?php

namespace Su\RestaurantBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class SpecialoffersFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('specialOfferId', 'filter_number_range')
            ->add('specialOfferTitle', 'filter_text')
            ->add('specialOfferDescription', 'filter_text')
            ->add('specialOfferPrice', 'filter_text')
            ->add('specialOfferDish', 'filter_number_range')
            ->add('specialOfferBanner', 'filter_text')
            ->add('specialOfferStatus', 'filter_text')

            //->add('creationDate', 'filter_date_range')
            //->add('updateDate', 'filter_date_range')
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ($event->getData() as $data) {
                if(is_array($data)) {
                    foreach ($data as $subData) {
                        if(null !== $subData) return;
                    }
                }
                else {
                    if(null !== $data) return;
                }
            }

            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_BIND, $listener);
    }

    public function getName()
    {
        return 'su_restaurantbundle_specialoffersfiltertype';
    }
}
