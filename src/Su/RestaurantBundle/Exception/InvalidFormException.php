<?php
/**
 * Created by PhpStorm.
 * User: TECH_2015-12-01
 * Date: 27/09/16
 * Time: 11:29 AM
 */

namespace Su\RestaurantBundle\Exception;


class InvalidFormException extends \RuntimeException
{
    protected $form;
    public function __construct($message, $form = null)
    {
        parent::__construct($message);
        $this->form = $form;
    }
    /**
     * @return array|null
     */
    public function getForm()
    {
        return $this->form;
    }

}