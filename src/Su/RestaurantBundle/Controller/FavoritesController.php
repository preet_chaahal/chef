<?php

namespace Su\RestaurantBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrapView;

use Su\RestaurantBundle\Entity\Favorites;
use Su\RestaurantBundle\Form\FavoritesType;
use Su\RestaurantBundle\Form\FavoritesFilterType;
//use Su\UserBundle\Controller\Users;
//use Su\UserBundle\Form\UserType;

/**
 * Favorites controller.
 *
 * @Route("/favorites")
 */
class FavoritesController extends Controller
{
    /**
     * Lists all Favorites entities.
     *
     * @Route("/", name="favorites")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        list($filterForm, $queryBuilder) = $this->filter();

        list($entities, $pagerHtml) = $this->paginator($queryBuilder);

        return array(
            'entities' => $entities,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createForm(new FavoritesFilterType());
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SuRestaurantBundle:Favorites')->createQueryBuilder('e');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('FavoritesControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->bind($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('FavoritesControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('FavoritesControllerFilter')) {
                $filterData = $session->get('FavoritesControllerFilter');
                $filterForm = $this->createForm(new FavoritesFilterType(), $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder)
    {
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $currentPage = $this->getRequest()->get('page', 1);
        $pagerfanta->setCurrentPage($currentPage);
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me)
        {
            return $me->generateUrl('favorites', array('page' => $page));
        };

        // Paginator - view
        $translator = $this->get('translator');
        $view = new TwitterBootstrapView();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => $translator->trans('views.index.pagprev', array(), 'JordiLlonchCrudGeneratorBundle'),
            'next_message' => $translator->trans('views.index.pagnext', array(), 'JordiLlonchCrudGeneratorBundle'),
        ));

        return array($entities, $pagerHtml);
    }

    /**
     * Creates a new Favorites entity.
     *
     * @Route("/", name="favorite_create")
     * @Method("POST")
     * @Template("SuRestaurantBundle:Favorites:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity   = new Favorites();
        //$entity_cl= new Users();
        $form = $this->createForm(new FavoritesType(), $entity);
        //$form2= $this->createForm(new UserType(), $entity_cl);
        $form->bind($request);

        if ($form->isValid()) {
            //
            //$d_client = $entity_cl->getClient();
            //$entity->setFavoriteClient($d_client->getClientId());
            //$em->persist($entity_cl);
            //$entity->setUsers($entity_cl->getId());
            
			//setFavoriteDish
			$d_dish = $entity->getDish();
            $entity->setFavoriteDish($d_dish->getDishId());
			
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            //return $this->redirect($this->generateUrl('favorite_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Favorites entity.
     *
     * @Route("/new", name="favorite_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Favorites();
        $form   = $this->createForm(new FavoritesType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Favorites entity.
     *
     * @Route("/{id}", name="favorite_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuRestaurantBundle:Favorites')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Favorites entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Favorites entity.
     *
     * @Route("/{id}/edit", name="favorite_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuRestaurantBundle:Favorites')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Favorites entity.');
        }

        $editForm = $this->createForm(new FavoritesType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Favorites entity.
     *
     * @Route("/{id}", name="favorite_update")
     * @Method("PUT")
     * @Template("SuRestaurantBundle:Favorites:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity   = $em->getRepository('SuRestaurantBundle:Favorites')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Favorites entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new FavoritesType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            //
			//setFavoriteDish
			$d_dish = $entity->getDish();
            $entity->setFavoriteDish($d_dish->getDishId());

            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            //return $this->redirect($this->generateUrl('favorite_edit', array('id' => $id)));
			return $this->redirect($this->generateUrl('favorites'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'flash.update.error');
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Favorites entity.
     *
     * @Route("/{id}", name="favorite_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SuRestaurantBundle:Favorites')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Favorites entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'flash.delete.error');
        }

        return $this->redirect($this->generateUrl('favorites'));
    }

    /**
     * Creates a form to delete a Favorites entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
