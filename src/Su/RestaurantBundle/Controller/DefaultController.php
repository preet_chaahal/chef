<?php

namespace Su\RestaurantBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/De")
     */
    public function indexAction()
    {
        return $this->render('SuRestaurantBundle:Default:index.html.twig');
    }
}
