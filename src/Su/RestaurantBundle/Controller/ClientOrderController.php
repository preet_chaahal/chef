<?php

namespace Su\RestaurantBundle\Controller;

//use Su\RestaurantBundle\Entity\ClientAddress;
//use Su\RestaurantBundle\Entity\DeliveryOrder;
//use Su\RestaurantBundle\Entity\OrderDish;
//use Su\RestaurantBundle\Form\ClientAddressType;
//use Su\RestaurantBundle\Form\OrderDishType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrapView;

use Su\RestaurantBundle\Entity\ClientOrder;
use Su\RestaurantBundle\Form\ClientOrderType;
use Su\RestaurantBundle\Form\ClientOrderFilterType;

/**
 * ClientOrder controller.
 *
 * @Route("/clientorder")
 */
class ClientOrderController extends Controller
{
    /**
     * Lists all ClientOrder entities.
     *
     * @Route("/", name="clientorder")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        list($filterForm, $queryBuilder) = $this->filter();

        list($entities, $pagerHtml) = $this->paginator($queryBuilder);


        //dump($entities->orderDish->getDish());

        return array(
            'entities' => $entities,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
        );
    }
       public function orderAction()
{
 list($filterForm, $queryBuilder) = $this->filter();

        list($entities, $pagerHtml) = $this->paginator($queryBuilder);
        return $this->render('SuRestaurantBundle:clientorder:order.html.twig',
          array(
            'entities' => $entities,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
        ));
    
}
    

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createForm(new ClientOrderFilterType());
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->getRepository('SuRestaurantBundle:ClientOrder')->createQueryBuilder('e');
                            //->Join('e.orderDish', 'od')
                            //->Join('od.clientAddress', 'ca');


        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('ClientOrderControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->bind($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('ClientOrderControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('ClientOrderControllerFilter')) {
                $filterData = $session->get('ClientOrderControllerFilter');
                $filterForm = $this->createForm(new ClientOrderFilterType(), $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder)
    {
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $currentPage = $this->getRequest()->get('page', 1);
        $pagerfanta->setCurrentPage($currentPage);
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me)
        {
            return $me->generateUrl('clientorder', array('page' => $page));
        };

        // Paginator - view
        $translator = $this->get('translator');
        $view = new TwitterBootstrapView();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => $translator->trans('views.index.pagprev', array(), 'JordiLlonchCrudGeneratorBundle'),
            'next_message' => $translator->trans('views.index.pagnext', array(), 'JordiLlonchCrudGeneratorBundle'),
        ));

        return array($entities, $pagerHtml);
    }

    /**
     * Creates a new ClientOrder entity.
     *
     * @Route("/", name="clientorder_create")
     * @Method("POST")
     * @Template("SuRestaurantBundle:ClientOrder:new.html.twig")
     */
    public function createAction(Request $request)
    {
        //dump($request);die('Stop Here Jooooooooooor....');
        $entity         = new ClientOrder();
         $userId         = ""; 
        //$client_address = new ClientAddress();
        //$orderDish      = new OrderDish();
        //$delivery       = new DeliveryOrder();

        $form           = $this->createForm(new ClientOrderType(), $entity);
        //$form2          = $this->createForm(new ClientAddressType(), $client_address);
        //$form3          = $this->createForm(new OrderDishType(), $orderDish);

        $form->bind($request);
        //$form2->bind($request);
        //$form3->bind($request);

        //if ($form->isValid() && $form2->isValid() && $form3->isValid()) {
        if ($form->isValid()) {
             $user    = $this->getUser();
            if($user){
              $userId = $user->getId(); 
              $entity->setClientId($userId);
            }
            $em = $this->getDoctrine()->getManager();
            
            $d_orderdish = $entity->getOrderDishId();
            $entity->setOrderDishId($d_orderdish->getId());
			
            /*$em->persist($client_address);
            $em->flush();
            $orderDish->setClientAddress($client_address);
            $em->persist($orderDish);
            $em->flush();
            $entity->setOrderDish($orderDish);
            */
            $em->persist($entity);
            $em->flush();


            //Update delivery order
            /*$delivery->setAddress($client_address);
            if( !empty( $client_address->getClientId() ) )
                $delivery->setClient($client_address->getClientId());
            $delivery->setClientOrder($entity);
            $em->persist($delivery);
            $em->flush();
            */
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            return $this->redirect($this->generateUrl('clientorder_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new ClientOrder entity.
     *
     * @Route("/new", name="clientorder_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity            = new ClientOrder();
        //$oderAddress       = new ClientAddress();
        //$orderDish         = new OrderDish();
        $form              = $this->createForm(new ClientOrderType(), $entity);
       // $formOderAddress   = $this->createForm(new ClientAddressType(), $oderAddress);
        //$formDish          = $this->createForm(new OrderDishType(),$orderDish);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
           // 'formAddress' => $formOderAddress->createView(),
            //'formDish' => $formDish->createView()
        );
    }

    /**
     * Finds and displays a ClientOrder entity.
     *
     * @Route("/{id}", name="clientorder_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuRestaurantBundle:ClientOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClientOrder entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ClientOrder entity.
     *
     * @Route("/{id}/edit", name="clientorder_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SuRestaurantBundle:ClientOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClientOrder entity.');
        }
//dump($entity->getOrderDish()->getClientAddress());die;
        $editForm              = $this->createForm(new ClientOrderType(), $entity);
        //$editFormOderAddress   = $this->createForm(new ClientAddressType(), $entity->getOrderDish()->getClientAddress());
        //$editFormDish          = $this->createForm(new OrderDishType(),$entity->getOrderDish());
        //$deleteForm            = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            //'edit_form_OderAddress'   => $editFormOderAddress->createView(),
            //'edit_form_Dish'   => $editFormDish->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing ClientOrder entity.
     *
     * @Route("/{id}", name="clientorder_update")
     * @Method("PUT")
     * @Template("SuRestaurantBundle:ClientOrder:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
//dump($request);die;
        $entity = $em->getRepository('SuRestaurantBundle:ClientOrder')->find($id);
        //$entityAddress = $em->getRepository('SuRestaurantBundle:ClientAddress')->find($entity->getOrderDish()->getClientAddress()->getId());
        //$entityDish    = $em->getRepository('SuRestaurantBundle:OrderDish')->find($entity->getOrderDish()->getId());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClientOrder entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm              = $this->createForm(new ClientOrderType(), $entity);
        //$editFormOderAddress   = $this->createForm(new ClientAddressType(), $entityAddress);
        //$editFormDish          = $this->createForm(new OrderDishType(),$entityDish);
        $editForm->bind($request);
        //$editFormOderAddress->bind($request);
        //$editFormDish->bind($request);

        if ($editForm->isValid() && $editFormOderAddress->isValid() && $editFormDish->isValid() ) {
            //$em->persist($entityAddress);
            //$em->flush();
            //$entity->getOrderDish()->setClientAddress($entity->getOrderDish()->getClientAddress());
            //$em->persist($entityDish);
            //$em->flush();
           // $entity->setOrderDish($entity->getOrderDish());
            $d_orderdish = $entity->getOrderDishId();
            $entity->setOrderDishId($d_orderdish->getId());
            
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            //return $this->redirect($this->generateUrl('clientorder_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('clientorder'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'flash.update.error');
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            //'edit_form_OderAddress'   => $editFormOderAddress->createView(),
            //'edit_form_Dish'   => $editFormDish->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a ClientOrder entity.
     *
     * @Route("/{id}", name="clientorder_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SuRestaurantBundle:ClientOrder')->find($id);
            //$entityAddress = $em->getRepository('SuRestaurantBundle:ClientAddress')->find($entity->getOrderDish()->getClientAddress()->getId());
            //$entityDish    = $em->getRepository('SuRestaurantBundle:OrderDish')->find($entity->getOrderDish()->getId());


            //if (!$entity && !$entityAddress && !$entityDish) {
            if (!$entity) {
                //throw $this->createNotFoundException('Unable to find ClientOrder or ClientAddress or OrderDish  entity.');
                throw $this->createNotFoundException('Unable to find ClientOrder entity.');
            }

            //$em->remove($entityAddress);
            //$em->remove($entityDish);
            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'flash.delete.error');
        }

        return $this->redirect($this->generateUrl('clientorder'));
    }

    /**
     * Creates a form to delete a ClientOrder entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
