<?php

namespace Su\FrontOfficeBundle\Controller;
 
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Su\RestaurantBundle\Entity\Dish;
 
class AutocompleteController extends Controller
{
    public function indexAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('SuRestaurantBundle:Dish')
          ;
 
        $listDish = $repository->findBy(
            array(),                      // Critere
            array('id' => 'desc'),        // Tri
            null,                         // Limite
            null                          // Offset
          );
 
        
      return $this->render('SuFrontOfficeBundle:Autocomplete:index.html.twig', array(
          'listDsih' => $listDish,
      ));
      
    }
}