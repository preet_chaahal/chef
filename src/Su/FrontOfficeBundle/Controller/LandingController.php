<?php

namespace Su\FrontOfficeBundle\Controller;

use Su\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Su\RestaurantBundle\Entity\Dish;
use Su\RestaurantBundle\Entity\Shop;
use Su\RestaurantBundle\Entity\SiteConfig;

use Su\RestaurantBundle\Entity\category;
use Su\RestaurantBundle\Entity\Specialoffers;

class LandingController extends Controller
{
    public function indexAction()
    {    
         $results_cat     = self::getTopMenuAction();//print_r($results_cat);
         $results_spe     = self::getMainBannersAction();//print_r($results_cat);
         $results_cate    = self::getCategoriesAction(); //print_r($results_cate);
         $shopObj         = $this->container->get('shop');
         $results_fav= $shopObj->subFavoritesAction();//print_r($results_cat);die();
        // print_r($results_fav);
        $cart_summ    = array();
        $session      = new Session();
        $itemCart     = $session->get('itemCart'); //Get existing cart items
        //print_r($itemCart);
        $landingObj   = $this->container->get('landing');
        $results_cat  = $landingObj->getTopMenuAction();//print_r($results_cat);die()
        $cartObj          = $session->get('cart');
        $num_cart_items   = $cartObj;
         return $this->render('SuFrontOfficeBundle:Landing:index.html.twig', 
         array(
         'entity_cat'      => $results_cat,
         'entity_specials' => $results_spe,
         'entity_fav'      => $results_fav,
         'entity_cate'     => $results_cate,
         'num_cart_items'  => $num_cart_items,
  
        ));
    }
    
    //Get top menu
    public function getTopMenuAction(){
        
         $limit_cat       = 6;
         $em              = $this->getDoctrine()->getManager();
         //Use this for the top menu
         $entity_cat      = $em->createQuery('SELECT c.categoryId, c.name FROM SuRestaurantBundle:Category c 
                                              ORDER BY c.categoryId ASC'); 
                            $entity_cat->setMaxResults($limit_cat);
         $results_cat     = $entity_cat->getResult();//print_r($results_cat);
         return $results_cat;
    }   
    
    //Get Main banners
    public function getMainBannersAction(){
        
         $em              = $this->getDoctrine()->getManager();
          //Use this for the rolling banners
         $entity_specials = $em->createQuery('SELECT so.specialOfferId, so.specialOfferTitle, so.specialOfferDescription, so.specialOfferPrice,
                                              so.specialOfferDish, so.specialOfferBanner,so.specialOfferStatus FROM SuRestaurantBundle:Specialoffers so
                                              WHERE so.specialOfferStatus>=1 ORDER BY so.specialOfferId DESC');
         $results_spe     = $entity_specials->getResult();
         return $results_spe;
    }  
    
    //Get Categories
  
    public function getCategoriesAction(){
        
         $results_cat     = array();
         $results_cat_arr = array();
         $cat_conter      = 0;
         $results_cat_val = array();
         $cat_id          = "";
         $cat_name        = "";
         $results_cat_dish= array();
        
         $em              = $this->getDoctrine()->getManager();
         //Use this for Dish categories
         $entity_cat      = $em->createQuery('SELECT c.categoryId, c.name FROM SuRestaurantBundle:Category c 
                                              ORDER BY c.categoryId ASC'); 
         $results_cat_arr     = $entity_cat->getResult();//print_r($results_cat);
         //loop through the results and merge with corresponding dishes - for appropriate dishes
         foreach($results_cat_arr as $results_val){
           $cat_id   = $results_val['categoryId'];
           $cat_name = $results_val['name'];
           //echo "FAV ID..".$fav_id;echo "<br/>";
           $entity_cat_dish = $em->createQuery('SELECT d.dishId, d.name, d.description, d.price, d.image, d.shop
                                                FROM SuRestaurantBundle:Dish d
                                                WHERE d.category='.$cat_id.' ORDER BY d.dishId DESC');
           $results_cat_dish = $entity_cat_dish->getResult();
           foreach($results_cat_dish as $results_cat_val){
             $results_cat_val['categoryId'] = $cat_id;
             $results_cat_val['catName']    = $cat_name;
             $results_cat[$cat_conter]      = $results_cat_val;
           }
           $cat_conter++;
         }
         return $results_cat;
    }    
     
}
