<?php

namespace Su\FrontOfficeBundle\Controller;

use Su\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\HttpFoundation\JsonResponse;
use Su\RestaurantBundle\Entity\Dish;

class AjaxAutocompleteController extends Controller
{
    public function updateDataAction(Request $request)
    {   
        //session_start();
        $data = $request->get('input');
        //$_SESSION['data'] = $data;
        //$data =$_SESSION['data'];
        //$data = 'Sa';
        //echo "DATA...". $data;//die();
        $em = $this->getDoctrine()->getManager();
        
        $query = $em->createQuery(''
                . 'SELECT d.dishId, d.name, d.image, d.category '
                . 'FROM SuRestaurantBundle:Dish d ' 
                . 'WHERE d.name LIKE :data '
                . 'ORDER BY d.name ASC'
                )
                ->setParameter('data', '%' . $data . '%');
                //print_r($query); die();
        $results = $query->getResult();
        //print_r($results); die();
        $dishList = '<ul id="matchList" style="border:0px solid #fff; margin-top:2px; padding-left:0px; background:#E0A71E;border-radius: 10px;">';
        if (count($results) > 0){
            foreach ($results as $result) { //echo $result['dishId']; die();
                $dishid   = $result['dishId'];
                $dishCategoryId = $result['category'];
                //$matchStringBold = preg_replace('/('.$data.')/i', '<strong>$1</strong>', utf8_decode($result['name'])); // Replace text field input by bold one
                //$dishList .= '<li id="'.$result['name'].'">'.$matchStringBold.'</li>'; // Create the matching list - we put maching name in the ID too
                $dishList .= '<li id="'.$result['name'].'" style="border-bottom:1px solid #1B1B1B; padding:5px;"><a href="'. $this->get('router')->generate("front_office_user_dishesbycat", array('id' => $dishCategoryId)) .'" style="color:#1B1B1B">'.utf8_decode($result['name']).'</a></li>';
            }
        }else{
            $dishList .= '<li id="" style="border-bottom:1px solid #1B1B1B; padding:5px;"><span style="color:#1B1B1B" >'.utf8_decode('&#9785; No result found!').'</span></li>';
        }
        $dishList .= '</ul>';
        //print_r($dishList); die();
        $response = new JsonResponse();
        $response->setData(array('dishList' => $dishList));
        //$response = new Response(json_encode(array('dishList' => $results)));
        /*
        $response = new JsonResponse(array('dishList' => $results));
        $response->headers->set('Content-Type', 'application/json');
        */
        //print_r($response);die();
        return $response;       
    }

    public function updateNewsletterAction(Request $request)
    {   
        $email = $request->get('email');
        $em = $this->getDoctrine()->getManager()->getConnection();
        $query = $em->insert('newsletter', array("email" => $email) );
        $results = $query;
        if($results){
            $data = array(
                'status' => 1,
                'message' => '<div style="clear: both"></div><span style="position: absolute; display: inline-block; background:#E0A71E; padding: 5px; font-size: 14px; color: white !important;">Email added to newsletter subscription list!</span>'
            );
        }else{
            $data = array(
                'status' => 0,
                'message' => 'Something went wrong trying to add email to newsletter subscription list!'
            );
        }
        $response = new JsonResponse();
        $response->setData($data);

        return $response;       
    }
}