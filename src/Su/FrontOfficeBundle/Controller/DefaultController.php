<?php

namespace Su\FrontOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SuFrontOfficeBundle:Default:index.html.twig');
    }
}
