<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('su_front_office_homepage', new Route('/', array(
    '_controller' => 'SuFrontOfficeBundle:Landing:index',
)));

return $collection;
