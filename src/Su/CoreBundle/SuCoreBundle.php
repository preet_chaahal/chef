<?php

namespace Su\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SuCoreBundle extends Bundle
{
    public function getParent()
    {
        return 'JordiLlonchCrudGeneratorBundle';
    }
}
