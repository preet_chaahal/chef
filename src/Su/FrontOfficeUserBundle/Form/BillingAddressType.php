<?php

namespace Su\FrontOfficeUserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BillingAddressType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
               
               ->add('clientName', 'text', array('label' => ' ', 'attr' => array('class' => 'address_form', 'placeholder' => 'Full Name')))
                //->add('address1', 'textarea', array('label' => ' ', 'attr' => array('cols' => '5', 'rows' => '5', 'placeholder' => 'Address', 'class' => 'address_form')))
                ->add('address1', 'textarea', array('label' => ' ', 'required' => false, 'attr' => array('cols' => '5', 'rows' => '5', 'placeholder' => 'Other Address', 'class' => 'address_form')))
                ->add('city', 'text', array('label' => ' ', 'attr' => array('class' => 'address_form', 'placeholder' => 'City')))
                ->add('zipcode', 'text', array('label' => ' ', 'attr' => array('class' => 'address_form', 'placeholder' => 'Zipcode')))
                
                ->add('email', 'email', array('label' => ' ', 'attr' => array('class' => 'address_form', 'placeholder' => 'Email')))
                ->add('phone', 'text', array('label' => ' ', 'attr' => array('class' => 'address_form', 'placeholder' => 'Phone')))
               // ->add('clientId', 'text', array('label' => ' ', 'attr' => array('class' => 'address_form','placeholder' => 'Latitude')))
                ->add('nickname', 'text', array('label' => ' ', 'attr' => array('class' => 'address_form', 'placeholder' => 'Longitude')))
                ->add('address2', 'text', array('label' => ' ', 'attr' => false, 'attr' => array( 'placeholder' => 'Latitude', 'class' => 'address_form')))
              

//->add('creationDate')
        //->add('updateDate')

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Su\RestaurantBundle\Entity\ClientAddress',
            'csrf_protection' => false,
        ));
    }

    public function getName() {
        return 'su_frontofficeuserbundle_billingaddress';
    }

}

?>
                            <br>
<!--                            <input type="text"  name="su_frontofficeuserbundle_billingaddress[long]" id="lng" readonly="" placeholder="Longitude" class="form-control"><br>
                             <input type="text"  name="su_frontofficeuserbundle_billingaddress[lat]" id="lat" readonly="" placeholder="Latitude" class="form-control col-md-7 col-xs-12"><br>-->
                            

