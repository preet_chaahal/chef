<?php

namespace Su\FrontOfficeUserBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PaymentDetailBilling extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                /* ->add('clientId', EntityType::class, array(
                  'label' => 'User Account',
                  'class' => 'SuUserBundle:User',
                  'placeholder' => 'Choose User Account',
                  'required' => false,
                  'empty_data'  => null,
                  'choice_label' => 'fullName',
                  'choice_value' => 'id',
                  )) */
                //->add('clientId', 'text', array('label' => ' ', 'attr' => array('class' => 'payment_form', 'placeholder' => 'Client Id')))
                ->add('payment_id', 'text', array('label' => ' ', 'attr' => array('class' => 'payment_form', 'placeholder' => 'Payment Id')))
                ->add('payment_key', 'text', array('label' => ' ', 'attr' => array('class' => 'payment_form', 'placeholder' => 'Payment Key')))
                ->add('payment_type', ChoiceType::class, array(
                    'choices' => array(
                        'Paypal Payment Gateway' => 'Paypal Payment Gateway',
                        'Stripe Payment Gateway' => 'Stripe Payment Gateway',
                        'Bitcoin Payment Gateway' => 'Bitcoin Payment Gateway',
                    ),
                    'preferred_choices' => array('muppets', 'arr')
        ));
//->add('creationDate')
        //->add('updateDate')

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Su\RestaurantBundle\Entity\PaymentDetailAddress',
            'csrf_protection' => false,
        ));
    }

    public function getName() {
        return 'su_frontofficeuserbundle_billingaddress';
    }

}
?>
<br>
<!--                            <input type="text"  name="su_frontofficeuserbundle_billingaddress[long]" id="lng" readonly="" placeholder="Longitude" class="form-control"><br>
 <input type="text"  name="su_frontofficeuserbundle_billingaddress[lat]" id="lat" readonly="" placeholder="Latitude" class="form-control col-md-7 col-xs-12"><br>-->


