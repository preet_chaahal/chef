<?php

namespace Su\FrontOfficeUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Su\RestaurantBundle\Entity\Dish;
use Su\RestaurantBundle\Entity\Shop;
use Su\RestaurantBundle\Entity\Favorites;

class ShopController extends Controller
{

    /**
     * @Route("/", name="front_office_user_homepage")
     *
     */
    public function shopAction(Request $request)
    {
       $landingObj   = $this->container->get('landing');
       $results_cat  = $landingObj->getTopMenuAction();//print_r($results_cat);die();
       $results_cate = $landingObj->getCategoriesAction(); //print_r($results_cate);
       $results_fav  = self::subFavoritesAction();//print_r($results_fav);die();

       return $this->render('SuFrontOfficeUserBundle:Shop:index.html.twig',
         array(
        'entity_cat'      => $results_cat,
        'entity_fav'      => $results_fav,
        'entity_cate'     => $results_cate,
       ));
    }

    /**
     * @Route("/dishes/{id}", name="front_office_user_dishesbycat")
     *
     */
    public function dishesByCatAction(Request $request)
    {
       $dish_id          = "";
       $dish_cat         = "";
       $dish_conter      = 0;
       $results_dish_arr = array();
       $results_dish_cat = array();
       $results_cdishes  = array();
       $is_favorite      = false;
       $em               = $this->getDoctrine()->getManager();

       $landingObj       = $this->container->get('landing');
       $results_cat      = $landingObj->getTopMenuAction();//print_r($results_cat);die();
       $results_cate = $landingObj->getCategoriesAction(); //print_r($results_cate);
       $results_fav      = self::subFavoritesAction();//print_r($results_fav);die();
       //print_r($request->get('id')); die();

       $dish_cat         = $request->get('id');

       /*
       if( ($dish_cat== "") || ((is_int($dish_cat) == false)) ){ //
          return  $this->forward('SuFrontOfficeUserBundle:Shop:index.html.twig');
       }else{
        */
       //Category Name
       $entity_dish_cat  = $em->createQuery('SELECT c.name
                                               FROM SuRestaurantBundle:Category c
                                               WHERE c.categoryId='.$dish_cat.'');
       $results_dish_cat = $entity_dish_cat->getResult();
       // print_r($results_dish_cat);die();
       $cat_name =  $results_dish_cat[0]['name'];
       //get dishes the following category
       $entity_cdishes= $em->createQuery('SELECT d.dishId, d.name, d.description, d.price,
                                          d.image, d.category, d.shop FROM SuRestaurantBundle:Dish d
                                          WHERE d.category ='.$dish_cat.'  ORDER BY d.dishId ASC');
       $results_cdishes= $entity_cdishes->getResult();//print_r($results_cdishes);
       foreach($results_cdishes as $results_val){
         $dish_id   = $results_val['dishId'];
         $dish_shop = $results_val['shop'];
         //echo "FAV ID..".$fav_id;echo "<br/>";
         //check if this dish has been favorited and then set the is_favorite flag
         if(!empty($results_fav)){
            foreach($results_fav as $results_fav_val){
                if($dish_id == $results_fav_val['dishId']) {
                    $is_favorite = true;
                }
            }
         }
         $results_cdishes[$dish_conter]['isFavorite'] = $is_favorite;

         $entity_dish_shop = $em->createQuery('SELECT s.shopId, s.shopName
                                               FROM SuRestaurantBundle:Shop s
                                               WHERE s.shopId='.$dish_shop.'');
         $results_dish_shop= $entity_dish_shop->getResult();
         foreach($results_dish_shop as $results_dish_val){
             $results_cdishes[$dish_conter]['shopName']   = $results_dish_val['shopName'];
         }
         $dish_conter++;
       }
       //print_r($results_cdishes); die();
       return $this->render('SuFrontOfficeUserBundle:Shop:c_dishes.html.twig',
         array(
        'entity_dishes'   => $results_cdishes,
        'entity_cat'      => $results_cat,
        'entity_fav'      => $results_fav,
        'entity_cat_name' => $cat_name,
        'entity_cate'     => $results_cate
       ));
       //}
    }

    /**
     * @Route("/dish/{id}", name="front_office_user_dish")
     *
     */
    public function dishAction($id)
    {
       //echo "ID...".$id; die();
       //if(($id == "") || (empty($id))){
       //   $this->redirect('front_office_user_homepage');;
       //}else{
        $em = $this->getDoctrine()->getManager();

        $entity_dish = $em->getRepository('SuRestaurantBundle:Dish')->find($id);
        $prod_img    = $entity_dish->getImage();
        $landingObj       = $this->container->get('landing');
        $results_cat      = $landingObj->getTopMenuAction();
        $results_cate = $landingObj->getCategoriesAction(); //print_r($results_cate);
        //echo $prod_img;die();
    	   //getShop
		    $entity_shop = $em->getRepository('SuRestaurantBundle:Shop')->find($entity_dish->getShop());
        //print_r($entity_shop); die();
        if ((!$entity_dish) || (!$entity_shop)) {
            throw $this->createNotFoundException('Unable to find Dish entity.');
        }
        return $this->render('SuFrontOfficeUserBundle:Shop:dish.html.twig',
        array(
        'entity_dish' => $entity_dish,
        'entity_shop' => $entity_shop,
        'prod_img'    => $prod_img,
        'entity_cat'  => $results_cat,
        'entity_cate' => $results_cate
        ));
        //return $this->render('SuFrontOfficeUserBundle:Shop:dish.html.twig');
       //}


    }

    /**
     * @Route("/stores", name="front_office_user_stores")
     *
     */
    public function storesAction(Request $request)
    {
       $landingObj   = $this->container->get('landing');
       $results_cat  = $landingObj->getTopMenuAction();//print_r($results_cat);die();
       $results_fav  = self::subFavoritesAction();//print_r($results_fav);die();
       $results_store= self::getStoresAction();//print_r($results_store);die();

       return $this->render('SuFrontOfficeUserBundle:Shop:stores.html.twig',
         array(
        'entity_cat'      => $results_cat,
        'entity_fav'      => $results_fav,
        'entity_store'    => $results_store,
       ));
    }

    //Get Stores
    public function getStoresAction(){

         $em              = $this->getDoctrine()->getManager();
         //Use this for the top menu
         $entity_stores   = $em->createQuery('SELECT s.shopId, s.shopName, s.shopDescription, s.shopLogo,
                                              s.shopLocation, s.shopZipcode FROM SuRestaurantBundle:Shop s
                                              ORDER BY s.shopId ASC');
         $results_stores  = $entity_stores->getResult();//print_r($results_stores);
         return $results_stores;
    }

    /**
     * @Route("/stores/dishes/{id}", name="front_office_user_dishesbystore")
     *
     */
    public function dishesByStoreAction(Request $request)
    {
       $dish_id            = "";
       $dish_store         = "";
       $dish_conter        = 0;
       $is_favorite        = false;
       $results_dish_arr   = array();
       $results_dish_store = array();
       $results_sdishes    = array();

       $em                 = $this->getDoctrine()->getManager();

       $landingObj = $this->container->get('landing');
       $results_cat= $landingObj->getTopMenuAction();//print_r($results_cat);die();
       $results_fav= self::subFavoritesAction();//print_r($results_fav);die();
       $dish_store         = $request->get('id');
       //Store Name
       $entity_dish_store  = $em->createQuery('SELECT s.shopName
                                               FROM SuRestaurantBundle:Shop s
                                               WHERE s.shopId='.$dish_store.'');
       $results_dish_store = $entity_dish_store->getResult();
       // print_r($results_dish_store);die();
       $shop_name          =  $results_dish_store[0]['shopName'];
       //get dishes the following category
       $entity_sdishes     = $em->createQuery('SELECT d.dishId, d.name, d.description, d.price,
                                               d.image, d.category, d.shop FROM SuRestaurantBundle:Dish d
                                               WHERE d.shop ='.$dish_store.'  ORDER BY d.dishId ASC');
       $results_sdishes= $entity_sdishes->getResult();//print_r($results_sdishes);
       foreach($results_sdishes as $results_val){
         $dish_id   = $results_val['dishId'];
         $dish_shop = $results_val['shop'];

         //check if this dish has been favorited and then set the is_favorite flag
         if(!empty($results_fav)){
            foreach($results_fav as $results_fav_val){
                if($dish_id == $results_fav_val['dishId']) {
                    $is_favorite = true;
                }
            }
         }
         $results_sdishes[$dish_conter]['isFavorite'] = $is_favorite;
         $results_sdishes[$dish_conter]['shopName']   = $shop_name;


         $dish_conter++;
       }
       //print_r($results_sdishes); die();
       return $this->render('SuFrontOfficeUserBundle:Shop:s_dishes.html.twig',
         array(
        'entity_dishes'    => $results_sdishes,
        'entity_cat'       => $results_cat,
        'entity_fav'       => $results_fav,
        'entity_shop_name' => $shop_name,
       ));
    }

   /**
     * @Route("/favorites", name="front_office_user_favorites")
     *
     */
    public function favoritesAction(Request $request)
    {
       $user   = $this->getUser(); //use current user to pick favorite
       //print_r($user); die();
       $userId = $user->getId();
       $em     = $this->getDoctrine()->getManager();
       /*$entity_favorites = $em->createQuery('SELECT f.favoriteId, f.favoriteClient, f.favoriteDish
                                             FROM SuRestaurantBundle:Favorites f
                                             WHERE f.favoriteClient='.$userId.' ORDER BY f.favoriteId DESC');
       $results_fav      = $entity_favorites->getResult();*/
       $shopObj         = $this->container->get('shop');
       $results_fav= $shopObj->subFavoritesAction();

       $landingObj = $this->container->get('landing');
       $results_cate = $landingObj->getCategoriesAction(); //print_r($results_cate);
       $results_cat= $landingObj->getTopMenuAction();//print_r($results_cat);die();

       return $this->render('SuFrontOfficeUserBundle:Shop:favorites.html.twig',
         array(
        'entity_cat'      => $results_cat,
        'entity_fav'      => $results_fav,
        'entity_cate'      => $results_cate,
       ));
    }



    /**
     * @Route("/add/favorite/{id}", name="front_office_user_addfavorite")
     *
     */
    public function addFavoriteAction(Request $request)
    {
       $user         = $this->getUser(); //use current user to pick favorite
       $fav_dish     = $request->get('id');
       $current_date = new \DateTime("now"); //date("Y-m-d H:i:s");
       //print_r($user); die();
       if((!empty($user)) && ($fav_dish != "")){ //do only for logged in users
           $userId   = $user->getId();
           $favorite = new Favorites();
           $favorite->setFavoriteClient($userId);
           $favorite->setFavoriteDish($fav_dish);
           $favorite->setFavoriteCreatedate($current_date);
           if($favorite){
               $em = $this->getDoctrine()->getManager();
               $em->persist($favorite);
               $em->flush();
           }

           return $this->redirect($request->headers->get('referer'));

        }else{
           return $this->redirect($this->generateUrl('front_office_user_homepage'));
        }
    }


     /**
     * @Route("/sub/favorites", name="front_office_user_subfavorites")
     *
     */
    public function subFavoritesAction()
    {
       $fav_id          = "";
       $fav_dish        = "";
       $fav_conter      = 0;
       $results_fav_arr = array();
       $results_fav_dish= array();
       $results_fav     = array();

       $user       = $this->getUser(); //use current user to pick favorite
       //print_r($user); die();
       if(!empty($user)){
           $userId = $user->getId(); //echo "user..".$userId;die();
           $limit_fav  = 4;
           $em         = $this->getDoctrine()->getManager();
           $entity_favorites = $em->createQuery('SELECT f.favoriteId, f.favoriteClient, f.favoriteDish
                                                 FROM SuRestaurantBundle:Favorites f
                                                 WHERE f.favoriteClient='.$userId.' ORDER BY f.favoriteId DESC');
                                                 $entity_favorites->setMaxResults($limit_fav);
           $results_fav_arr  = $entity_favorites->getResult();

           //print_r($results_fav);
           //loop through the results and merge with corresponding dishes
           foreach($results_fav_arr as $results_val){
             $fav_id   = $results_val['favoriteId'];;
             $fav_dish = $results_val['favoriteDish'];
             //echo "FAV ID..".$fav_id;echo "<br/>";
             $entity_fav_dish = $em->createQuery('SELECT d.dishId, d.name, d.description, d.price, d.image, d.shop
                                                   FROM SuRestaurantBundle:Dish d
                                                   WHERE d.dishId='.$fav_dish.'');
             $results_fav_dish = $entity_fav_dish->getResult();
             foreach($results_fav_dish as $results_fav_val){
                 $results_fav_val['favoriteId'] = $fav_id;
                 $results_fav[$fav_conter]      = $results_fav_val;
             }
             $fav_conter++;
           }
       } //end if
       return $results_fav;
    }


}
