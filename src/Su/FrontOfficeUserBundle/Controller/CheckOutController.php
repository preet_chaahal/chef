<?php

/**
 * @author Femi Bello
 * @copyright 2017
 */

namespace Su\FrontOfficeUserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Su\RestaurantBundle\Entity\Dish;
use Su\RestaurantBundle\Entity\Shop;
use Su\RestaurantBundle\Entity\SiteConfig;
use Su\RestaurantBundle\Entity\Favorites;
use Su\RestaurantBundle\Entity\ClientAddress;
use Su\FrontOfficeUserBundle\Form\BillingAddressType;
use Su\FrontOfficeUserBundle\Form\BillingAddresFilterType;

/**
 * Controller responsible for Billing Address, Placing Order and Payment
 */
class CheckOutController extends Controller
{
    /**
     * Renders all Addresses that belongs to logged in user and presents form to create a new address
     *
     * @Route("/billing-address", name="front_office_user_address")
     */
    public function indexAction()
    {   
        $entity = new ClientAddress();
        $form   = $this->createForm(new BillingAddressType(), $entity);

        $userId       = ""; 
        $cart_summ    = array();
        $session      = new Session();
        $itemCart     = $session->get('itemCart'); //Get existing cart items
        //print_r($itemCart);
        $landingObj   = $this->container->get('landing');
        $results_cat  = $landingObj->getTopMenuAction();//print_r($results_cat);die()
        $billingObj           = $session->get('billing');      
        $mybillingaddresses   = array();
        $mybillingaddresses   = $billingObj;
        $cartObj              = $session->get('cart');
        $num_cart_items       = $cartObj;
        
  
        
        //echo "ITEMS IN CART...".$num_cart_items;//die();
        
        /*//check if user is logged in and include the user id in the request
        $user    = $this->getUser();
        if($user){
         $userId = $user->getId(); 
        }
        */
        if(!empty($itemCart)){
            $cart_summ['sub_total']     = 0;
            $cart_summ['tax']           = 0;
            $cart_summ['shipping_cost'] = 0;
            foreach($itemCart as $itemval){ //Summarize cart values
                $cart_summ['sub_total']      += ($itemval['dish_price'] * $itemval['qty']);
                $cart_summ['tax']            += $itemval['tax'];
                $cart_summ['shipping_cost']   = $itemval['delivery'];
            }
            $cart_summ['total']  =  $cart_summ['sub_total'] + $cart_summ['tax'] + $cart_summ['shipping_cost']; //get total
        }
        //print_r($mybillingaddresses);
        return $this->render('SuFrontOfficeUserBundle:Shop:address.html.twig', 
         array(
        'entity_cat'             => $results_cat,
        'entity_cart'            => $itemCart,
        'entity_summ'            => $cart_summ,
        'num_cart_items'         => $num_cart_items,
        'mybillingaddresses'     => $mybillingaddresses,
        'entity' => $entity,
        'form'   => $form->createView(),
        //'userId' => $userId,
       ));  
    }
    
    
    
     /**
     * Add Billing Address
     * 
     * @Route("/add-billing-address", name="front_office_user_createaddress")
     * @Method("POST")
     * @Template("SuFrontOfficeUserBundle:Shop:address.html.twig")
     */     
    public function addBillingAddressAction(Request $request)
    {  
       $userId         = ""; 
       $latest_address = "";
       $item_counter   = 0;
       $session        = new Session();
       $entity         = new ClientAddress();

       $form           = $this->createForm(new BillingAddressType(), $entity);       
       $form->bind($request);
       
       if ($form->isValid()) {
            //check if user is logged in and include the user id in the request
            $user    = $this->getUser();
            if($user){
              $userId = $user->getId(); 
              $entity->setClientId($userId);
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            //Get id of inserted address and make default address for current cart
            $latest_address = $entity->getId();
            //set address for item in cart
            $itemCart       = $session->get('itemCart'); //Get existing cart items
            if(!empty($itemCart)){
                foreach($itemCart as $itemval){
                    $itemCart[$item_counter]['billingaddress'] = $latest_address;
                    $item_counter++;
                }
                $session->clear('itemCart'); //clear all items in cart
                $session->set('itemCart', $itemCart); //set updated items to cart
            }
            
            $session->getFlashBag()->add('success', "Billing Address was created successfully");
       }

            
       return $this->redirect($this->generateUrl('front_office_user_address'));   
        
    }
    
     /**
     * Set an address as billing for an order
     * 
     * @Route("/set-billing-address", name="front_office_user_setaddress")
     * @Method("POST")
     * @Template("SuFrontOfficeUserBundle:Shop:address.html.twig")
     * 
     */     
    public function setBillingAddressAction(Request $request)
    {  
       $item_counter = 0;  
       $session      = new Session(); 
       $addressid    = $request->get('addressid'); //addressid
       $itemCart     = $session->get('itemCart'); //Get existing cart items
       if((!empty($itemCart))){
            foreach($itemCart as $itemval){
                $itemCart[$item_counter]['billingaddress'] = $addressid;
                $item_counter++;
            }
            $session->clear('itemCart'); //clear all items in cart
            $session->set('itemCart', $itemCart); //set updated items to cart
       }
       $session->getFlashBag()->add('success', "Address was set for your pending order");
       return $this->redirect($this->generateUrl('front_office_user_address'));   
        
    }
    
    
    /**
     * List Billing Addresses for Logged in user
     *
     */     
    public function getMyBillingAddressesAction()
    {  
       $userId       = "";
       $item_counter = 0; 
       $user         = $this->getUser();
            if($user){
              $userId = $user->getId(); 
       }
       $entity_billing_address = array();
       $results_billing_address= array();
       $session                = new Session();
       $em                     = $this->getDoctrine()->getManager();
       $itemCart               = $session->get('itemCart'); //Get existing cart items
       $entity_billing_address = $em->createQuery('SELECT ca.id, ca.clientName, ca.address1, ca.address2,
                                          ca.city, ca.zipcode, ca.nickname, ca.email, ca.phone FROM SuRestaurantBundle:ClientAddress ca 
                                          WHERE ca.clientId ='.$userId.'  ORDER BY ca.id DESC'); 
       $results_billing_address= $entity_billing_address->getResult();//print_r($results_billing_address);
       $itemCart       = $session->get('itemCart'); //Get existing cart items
       if((!empty($itemCart)) &&(!empty($results_billing_address))){
            foreach($itemCart as $itemval){
                $itemCart[$item_counter]['billingaddress'] = $results_billing_address[0]['id'];
                $item_counter++;
            }
            $session->clear('itemCart'); //clear all items in cart
            $session->set('itemCart', $itemCart); //set updated items to cart
       }
       
       return $results_billing_address;   
        
    }

  
    
}
