<?php

namespace Su\FrontOfficeUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SuFrontOfficeUserBundle:Login:index.html.twig');
    }
    
}
