<?php
/**
 * Created by PhpStorm.
 * User: phlema
 * Date: 01/10/2016
 * Time: 02:34 PM
 */

namespace Su\OAuth2Bundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\OAuthServerBundle\Controller\TokenController as BaseController;
use OAuth2\OAuth2;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TokenController extends BaseController
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param OAuth2 $server
     */
    public function __construct(OAuth2 $server, EntityManager $entityManager )
    {
        parent::__construct($server);
        $this->em = $entityManager;
    }


    public function tokenAction(Request $request)
    {
        // Do whatever you like here
        //dump($request);die;

        $result = parent::tokenAction($request);

        // More custom code.

        return $result;
    }
}