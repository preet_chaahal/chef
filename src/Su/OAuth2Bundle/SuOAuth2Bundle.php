<?php

namespace Su\OAuth2Bundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SuOAuth2Bundle extends Bundle
{
    public function getParent()
    {
        return 'FOSOAuthServerBundle';
    }
}
