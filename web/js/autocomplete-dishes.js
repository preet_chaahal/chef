 var timer;
    var timeout = 800;

     $("#myTextField").on('keyup', function() { // everytime keyup event
            clearTimeout(timer);
            $this = $(this);
            if ($(this).val) {
                timer = setTimeout(function(){
                    var input = $this.val(); // We take the input value
                    if ( input.length >= 2 ) { // Minimum characters = 2 (you can change)
                        $('#match').html('<img src="' + window.loader + '" />'); // Loader icon apprears in the <div id="match"></div>
                        var data = {input: input}; // We pass input argument in Ajax
                        $.ajax({
                             type: "POST",
                             url: ROOT_URL + "ajax/autocomplete/update/data", // call the php file ajax/tuto-autocomplete.php (check the routine we defined)
                             data: data, // Send dataFields var
                             // dataType: 'json', // json method
                             cache: false,
                             timeout: 24000,
                             success: function(response){ // If success
                                     $('#match').html(response.dishList); // Return data (UL list) and insert it in the <div id="match"></div>
                                     $('#matchList li').on('click', function() { // When click on an element in the list
                                            $('#myTextField').val($(this).text()); // Update the field with the new element
                                            // $('#match').html('<ul><li style="color: white">No match found!</li></ul>'); // Clear the <div id="match"></div>
                                     });     
                             },
                             error: function(xhr, status, error) { // if error
                                     $('#match').html("<ul><li style='color: white'>"+ error +"</li></ul>");
                             }
                        });
                    }
                }, timeout)        
            }
     });

$(document).ready(function(){

    $(".companyinfo form.searchform button[type=submit]").on('click', function(e) { // everytime keyup event
             e.preventDefault();
             var $this = $(this).parents('form');
             var email = $(this).parents('form').find('input[type=text]').val(); // We take the input value
             var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
             if ( re.test(email) ) { 
                     var data = {email: email}; // We pass input argument in Ajax
                     $.ajax({
                             type: "POST",
                             url: ROOT_URL + "ajax/autocomplete/update/newsletter", // call the php file ajax/tuto-autocomplete.php (check the routine we defined)
                             data: data, // Send dataFields var
                             dataType: 'json', // json method
                             cache: false,
                             timeout: 24000,
                             success: function(response){ // If success
                                    console.log(response);
                                    $this.append("<span style='color: green;'><br>"+ response.message +"<span>").find('span').delay(2000).fadeOut('slow');   
                             },
                             error: function(xhr, status, error) { // if error
                                    console.log(error);
                                    $this.append("<span style='color: yellow;'><br>Please try again later<span>").find('span').delay(2000).fadeOut('slow');
                             }
                     });
             } else {
                $this.append("<span style='color: red;'><br>Invalid email!<span>").find('span').delay(2000).fadeOut('slow');
             }
     });
});