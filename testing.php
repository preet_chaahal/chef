<?php
/**
 * Created by PhpStorm.
 * User: TECH_2015-12-01
 * Date: 26/09/16
 * Time: 12:15 PM
 */

require __DIR__.'/vendor/autoload.php';

$client = new \GuzzleHttp\Client([
    'base_url' => 'http://localhost:8000',
    'defaults' => [
        'exceptions' => false
    ]
]);

$response = $client->post('api/category');
echo $response;
echo "\n\n";
