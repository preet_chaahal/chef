THINGS TO LOOK OUT FOR IN THIS VERSION
----------------------
	1. API for Shops, Favorites and Special Offers  -- Done
	2. Loading Dishes , Dish detail Dynamically -- Done
	3. Search -- Done
	4. Special Offers (Banners) display dynamically --Done
	5. Creating Addresses -- Pending
	6. Shop Cart, Order and Payment Integration -- Pending (Bug)
	7. Adding a Dish as favorite -- Done
	8. Display Favorites Dynamically -- Done
	9. Display all categories Dynamically -- Done
	10. Display all stores Dynamically (from dishes) -- Done
	11. Dishes by Categories -- Done
	12. Dishes by Stores -- Done
	13. Loading top menu dynamically (from categories) -- Done
	14. Geolocation -- Pending



LATEST SQL CHANGES:
-------------------------------------------------
    I placed some sql files with test data in the *root/sqls folder for you to be able to run tests on the parts that comes with this version.
