-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2017 at 07:26 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sb_restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `specialoffers`
--

CREATE TABLE `specialoffers` (
  `special_offer_id` int(14) NOT NULL,
  `special_offer_title` varchar(255) NOT NULL,
  `special_offer_description` varchar(1000) DEFAULT NULL,
  `special_offer_price` double NOT NULL,
  `special_offer_dish` int(14) NOT NULL,
  `special_offer_banner` varchar(100) DEFAULT NULL,
  `special_offer_status` varchar(1) NOT NULL DEFAULT '1',
  `special_offer_createdate` datetime NOT NULL,
  `special_offer_lastupdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialoffers`
--

INSERT INTO `specialoffers` (`special_offer_id`, `special_offer_title`, `special_offer_description`, `special_offer_price`, `special_offer_dish`, `special_offer_banner`, `special_offer_status`, `special_offer_createdate`, `special_offer_lastupdate`) VALUES
(1, 'Pasta with Seafood', 'These delightful treats from the sea are key to a healthy diet. Low in calories, sodium, and cholesterol, protein-packed seafood provides vitamins and minerals. ', 12, 1, 'seafood1.jpg', '2', '2017-01-12 00:00:00', '2017-01-12 00:00:00'),
(2, 'Shrimp Salads', 'Cooked shrimp spritzed with fresh lemon and lime juices, then tossed with fresh dill, green onion, and celery for a cool, crunchy salad', 7, 2, 'shrimp2.jpg', '1', '2017-01-12 00:00:00', '2017-01-12 00:00:00'),
(3, 'Steak & Beef', 'Grilled, broiled, pan-fried, or slow-cooked. Plus marinades, sauces, gravies, and rubs to amp up the flavor.', 10, 3, 'beefsteak.jpg', '1', '2017-01-12 00:00:00', '2017-01-12 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `specialoffers`
--
ALTER TABLE `specialoffers`
  ADD PRIMARY KEY (`special_offer_id`),
  ADD KEY `special_offer_dish` (`special_offer_dish`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `specialoffers`
--
ALTER TABLE `specialoffers`
  MODIFY `special_offer_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
