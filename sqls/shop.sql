-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2017 at 09:35 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sb_restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE `shop` (
  `shop_id` int(14) NOT NULL,
  `shop_name` varchar(255) NOT NULL,
  `shop_description` longtext,
  `shop_logo` varchar(1000) DEFAULT 'shop_logo.jpg',
  `shop_location` varchar(400) NOT NULL,
  `shop_zipcode` varchar(50) NOT NULL,
  `shop_createdate` datetime NOT NULL,
  `shop_lastupdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`shop_id`, `shop_name`, `shop_description`, `shop_logo`, `shop_location`, `shop_zipcode`, `shop_createdate`, `shop_lastupdate`) VALUES
(1, 'Papa Johns', 'Papa Johns', 'papajohn_logo.jpg', 'Text Location', '000111', '2016-12-29 00:00:00', '2016-12-29 00:00:00'),
(2, 'Mi Pueblito', 'Mi Pueblito', 'pueblito_logo.jpg', 'Test Location', '000222', '2016-12-29 00:00:00', '2016-12-29 00:00:00'),
(3, 'El Rincon Boricua', 'El Rincon Boricua', 'rincon_logo.jpg', 'Test Location', '000111', '2017-01-15 00:00:00', '2017-01-15 00:00:00'),
(4, 'Loco Taqueria & Oyster Bar', 'Loco Taqueria & Oyster Bar', 'loco_logo.jpg', 'Test Location', '000111', '2017-01-15 00:00:00', '2017-01-15 00:00:00'),
(5, 'Neptune Oyster', 'Neptune Oyster', 'neptune_logo.png', '63 Salem St # 1, Boston, MA 02113, USA', '02113', '2017-01-15 00:00:00', '2017-01-15 00:00:00'),
(6, 'Bahama Breeze', 'Bahama Breeze', 'breeze_logo.png', 'Test Location', '000111', '2017-01-16 00:00:00', '2017-01-16 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`shop_id`),
  ADD UNIQUE KEY `name` (`shop_name`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `shop_id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
