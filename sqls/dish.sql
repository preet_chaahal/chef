-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2017 at 09:38 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sb_restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `dish`
--

CREATE TABLE `dish` (
  `dish_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `shop` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dish`
--

INSERT INTO `dish` (`dish_id`, `name`, `description`, `price`, `image`, `category`, `shop`, `creation_date`, `update_date`) VALUES
(1, 'Baked Oister', 'Baked Oister', 10.91, 'product1.jpg', 1, 1, '2016-09-25 15:16:11', '2016-10-05 11:01:58'),
(2, 'Seafood and Citiries', 'Seafood and Citiries', 50, 'product2.jpg', 2, 2, '2016-09-25 15:16:42', '2016-09-30 07:38:55'),
(3, 'Tuna and Moyonaise', 'Tuna and Moyonaise', 42, 'product3.jpg', 1, 1, '2016-09-30 01:00:40', '2016-10-10 04:00:43'),
(4, 'Shrimp and Avocado', 'Shrimp and Avocado', 70, 'product4.jpg', 3, 3, '2016-10-12 06:30:51', '2016-10-12 06:30:51'),
(5, 'Perperoni Pie', 'Perperoni Pie Alata sue sue', 10, 'gallery4.jpg', 4, 4, '2017-01-19 00:00:00', '2017-01-19 00:00:00'),
(6, 'Chocolate Nuts Ice Cream', 'Delicious Chocolate Nuts Ice Cream', 8, 'gallery5.jpg', 5, 5, '2017-01-19 00:00:00', '2017-01-19 00:00:00'),
(7, 'Chinese Elixir Soup', 'Chinese Elixir Soup Gives you a youthful booze', 15, 'gallery6.jpg', 6, 6, '2017-01-19 00:00:00', '2017-01-19 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dish`
--
ALTER TABLE `dish`
  ADD PRIMARY KEY (`dish_id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `category` (`category`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dish`
--
ALTER TABLE `dish`
  MODIFY `dish_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
